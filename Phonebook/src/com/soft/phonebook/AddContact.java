package com.soft.phonebook;

import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.constant.ResultCodes;
import com.soft.phonebook.database.DBOperations;
import com.soft.phonebook.design.BitmapUtils;
import com.soft.phonebook.design.CustomToast;
import com.soft.phonebook.design.Decoder;
import com.soft.phonebook.design.EditTextFormatted;

/**
 * Активность, создающая новый контакт и добавляющая его в базу
 * 
 * @author Nickolai
 * 
 */
public class AddContact extends Activity {

	// Создание объектов элементов управления
	Button save, cancel;
	ImageView picture;
	TextView name, surname, patronymic, phone;
	EditTextFormatted dateOfBirth;
	Spinner category;
	ImageButton createCategory;

	// по умолчанию картинка не выбрана
	String selectedPictureURI = "";

	/**
	 * Проверка на пустые поля
	 * 
	 * @return true - если поля заполнены
	 */
	boolean isValidated() {
		if (name.getText().toString().length() == 0) {
			name.setError("Укажите Имя");
			return false;
		}
		if (phone.getText().toString().length() == 0) {
			phone.setError("Укажите телефон");
			return false;
		}
		return true;
	}

	/*
	 * @Override(non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Указываем, что Activity будет без заголовка
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_add_contact);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.WHITE));

		// Инициализируем элементы управления
		save = (Button) findViewById(R.id.activity_add_contact_button_save);
		save.setOnClickListener(onSaveClickListener);
		cancel = (Button) findViewById(R.id.activity_add_contact_button_cancel);

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		createCategory = (ImageButton) findViewById(R.id.activity_add_contact_create_category);
		createCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(AddContact.this,
						CreateCategory.class), 1);
			}
		});

		picture = (ImageView) findViewById(R.id.activity_add_contact_photo);
		picture.setImageBitmap(BitmapUtils
				.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
						.decodeResource(AddContact.this.getResources(),
								R.drawable.no_image), 100));

		picture.setOnClickListener(pictureClickListener);

		name = (TextView) findViewById(R.id.activity_add_contact_name);
		surname = (TextView) findViewById(R.id.activity_add_contact_surname);
		phone = (TextView) findViewById(R.id.activity_add_contact_phone);
		patronymic = (TextView) findViewById(R.id.activity_add_contact_patronymic);
		dateOfBirth = (EditTextFormatted) findViewById(R.id.activity_add_contact_date_of_birth);
		dateOfBirth.setMask("dd.dd.dddd");

		category = (Spinner) findViewById(R.id.activity_add_contact_category);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, R.id.TextView1,
				DBOperations.getCategoriesList(this));
		adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		category.setPrompt("Выберите категорию");
		category.setAdapter(adapter);
		category.setSelection(0);
	}

	// Нажатие на картинку
	OnClickListener pictureClickListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"),
					ResultCodes.PICTURE_SELECTOR);

		}
	};

	OnClickListener onSaveClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (isValidated()) {
				// Если поля заполнены, то добавляем контакт в БД
				if (DBOperations.addContact(AddContact.this, name.getText()
						.toString(), surname.getText().toString(), patronymic
						.getText().toString(), phone.getText().toString(),
						category.getSelectedItem().toString(), dateOfBirth
								.getText().toString(), selectedPictureURI)) {
					CustomToast.getSuccessToast(AddContact.this, "Сохранено",
							Toast.LENGTH_SHORT).show();
				} else {
					CustomToast.getSuccessToast(AddContact.this, "Ошибка!",
							Toast.LENGTH_LONG).show();
				}
				// Возвращаем результат
				setResult(ResultCodes.CREATE_CONTACT);
				finish();
			}
		}
	};

	/**
	 * (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		// Результат от CreareCategory Activity
		case ResultCodes.CREATE_CATEGORY:
			System.out.println("CREATE CATEGORY ON RESULT");
			String newCategoryName = data.getExtras().getString(
					Constant.EXTRA_NEW_CATEGORY_NAME);
			DBOperations.addCategory(this, newCategoryName);

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					R.layout.spinner_item, R.id.TextView1,
					DBOperations.getCategoriesList(this));
			adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
			category.setPrompt("Выберите категорию");
			category.setAdapter(adapter);
			category.setSelection(adapter.getCount() - 1);

			break;
		default:
			break;
		}

		switch (requestCode) {
		// Результат от Picture Selector
		case ResultCodes.PICTURE_SELECTOR: {
			if (data != null) {
				// Забираем URI выбранной картинки
				String pictureURI = data.getDataString();
				System.out.println(pictureURI);
				selectedPictureURI = pictureURI;
				Uri uri = Uri.parse(pictureURI);
				try {
					picture.setImageBitmap(BitmapUtils
							.getCircleMaskedBitmapUsingPorterDuff(
									Decoder.decodeUri(AddContact.this, uri),
									500));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			break;
		}
		default:
			break;
		}
	}
}
