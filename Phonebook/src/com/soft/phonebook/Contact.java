package com.soft.phonebook;

import java.io.FileNotFoundException;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.design.BitmapUtils;
import com.soft.phonebook.design.Decoder;

/**
 * Активность, отобращающая контакт
 * 
 * @author Nickolai
 * 
 */
public class Contact extends ActionBarActivity {

	Button call;
	ImageButton sendSMS;
	TextView name, surname, patronymic, dateOfBirth;

	Intent extras;
	ImageView picture;

	String phoneNumber;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact);
		initActionBar();
		// получаем, переданные в эту активность данные
		extras = getIntent();

		call = (Button) findViewById(R.id.activity_contact_button_call);
		call.setOnClickListener(callClickListener);

		sendSMS = (ImageButton) findViewById(R.id.activity_contact_button_send_sms);
		sendSMS.setOnClickListener(sendSMSClickListener);

		name = (TextView) findViewById(R.id.activity_contact_name);
		surname = (TextView) findViewById(R.id.activity_contact_surname);
		patronymic = (TextView) findViewById(R.id.activity_contact_patronymic);
		dateOfBirth = (TextView) findViewById(R.id.activity_contact_date_of_birth);
		picture = (ImageView) findViewById(R.id.activity_contact_photo);
		// обновляем данные
		updateData(extras);
	}

	/**
	 * Инициализация Action Bar`а
	 * 
	 * @return ActionBar
	 */
	private android.support.v7.app.ActionBar initActionBar() {
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.phonebookstyle_color));
		return actionBar;
	}

	OnClickListener callClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			startActivity(new Intent(
					"android.intent.action.CALL",
					Uri.parse("tel:"
							+ extras.getStringExtra(Constant.EXTRA_CONTACT_PHONE))));
		}
	};

	OnClickListener sendSMSClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			startActivity(new Intent(Contact.this, SendSMSActivity.class)
					.putExtras(extras));
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contact, menu);
		MenuItem editItem = menu.findItem(R.id.contact_edit);
		editItem.setOnMenuItemClickListener(editClickListener);
		return true;
	}

	OnMenuItemClickListener editClickListener = new OnMenuItemClickListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.view.MenuItem.OnMenuItemClickListener#onMenuItemClick(android
		 * .view.MenuItem)
		 */
		@Override
		public boolean onMenuItemClick(MenuItem arg0) {
			startActivityForResult(
					new Intent(Contact.this, EditContact.class)
							.putExtras(extras),
					1);
			return false;
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (data != null) {
			updateData(data);
		}
	}

	/**
	 * Обновляет данные на экране.
	 * 
	 * @param data
	 *            Intent с данными
	 */
	private void updateData(Intent data) {
		// TODO Auto-generated method stub
		call.setText(data.getStringExtra(Constant.EXTRA_CONTACT_PHONE));
		name.setText(data.getStringExtra(Constant.EXTRA_CONTACT_NAME));
		surname.setText(data.getStringExtra(Constant.EXTRA_CONTACT_SURNAME));
		patronymic.setText(data
				.getStringExtra(Constant.EXTRA_CONTACT_PATRONYMIC));
		if (data.getStringExtra(Constant.EXTRA_CONTACT_DATE_OF_BIRTH).length() != 0) {
			dateOfBirth.setText(data
					.getStringExtra(Constant.EXTRA_CONTACT_DATE_OF_BIRTH));
		} else {
			dateOfBirth.setText("не указана");
		}
		picture.setImageBitmap(BitmapUtils
				.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
						.decodeResource(this.getResources(),
								R.drawable.no_image), 25));
		String selectedPictureURI = data
				.getStringExtra(Constant.EXTRA_CONTACT_PICTURE);
		if (selectedPictureURI.length() != 0) {
			try {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(
								Decoder.decodeUri(Contact.this,
										Uri.parse(selectedPictureURI)), 500));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		extras = data;
	};

}
