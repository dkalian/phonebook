package com.soft.phonebook.constant;

/**
 * Result коды
 * @author Nickolai
 *
 */
public class ResultCodes {
	public static final int CREATE_CATEGORY = 1;
	public static final int CREATE_CONTACT = 2;
	public static final int CATEGORY_ACTION_DELETE = 3;
	public static final int CATEGORY_ACTION_CHANGE = 4;
	public static final int EDIT_CONTACT = 5;
	public static final int CONTACT_SELECT_ACTION = 6;
	public static final int CONTACT_SELECT_ACTION_ACTION_TO_FAVORITES = 7;
	public static final int PICTURE_SELECTOR = 8;
}
