package com.soft.phonebook.constant;

/**
 * Константы
 * @author Nickolai
 *
 */
public class Constant {
	
	public static final String EXTRA_NEW_CATEGORY_NAME = "new_category_name";
	public static final String EXTRA_SELECTED_CATEGORY_NAME = "selected_category_name";

	public static final String EXTRA_CONTACT_ID = "contact_id";
	public static final String EXTRA_CONTACT_NAME = "contact_name";
	public static final String EXTRA_CONTACT_SURNAME = "contact_surname";
	public static final String EXTRA_CONTACT_PATRONYMIC = "contact_patronymic";
	public static final String EXTRA_CONTACT_PICTURE = "contact_picture";
	public static final String EXTRA_CONTACT_PHONE = "contact_phone";
	public static final String EXTRA_CONTACT_CATEGORY = "contact_category";
	public static final String EXTRA_CONTACT_DATE_OF_BIRTH = "contact_date_of_bidth";
	
	public static final String EXTRA_ERROR_CODE = "error_code";
	
	public static final String SHARED_PREFERENCES_NAME = "PhonebookSP";
	public static final String EXTRA_PASSWORD = "password_extra";
	public static String PREFERENCE_PASS = "password";
}
