package com.soft.phonebook;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;

/**
 * Активность с настройками
 * 
 * @author Николай
 * 
 */
public class Preference extends PreferenceActivity {
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Подгружаем разметку экрана из R.xml.preference
		addPreferencesFromResource(R.xml.preference);
		// Находим настройку с паролем
		android.preference.Preference category = findPreference("password");
		category.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see android.preference.Preference.OnPreferenceClickListener#
			 * onPreferenceClick(android.preference.Preference)
			 */
			@Override
			public boolean onPreferenceClick(android.preference.Preference arg0) {
				// Если нажали на настройку пароля, то запускаем активность со
				// мненой пароля
				startActivity(new Intent(Preference.this,
						ChangePasswordPreference.class));
				return false;
			}
		});
	}
}
