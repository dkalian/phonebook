package com.soft.phonebook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.constant.ResultCodes;
import com.soft.phonebook.database.DBOperations;

/**
 * Активность отображающая контекстное меню для контакта
 * 
 * @author Nickolai
 * 
 */
public class ContactSelectAction extends Activity {

	Button change, delete, toFavorites, cancel;
	Bundle extras;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_contact_select_action);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		change = (Button) findViewById(R.id.activity_contact_select_action_button_change);
		delete = (Button) findViewById(R.id.activity_contact_select_action_button_delete);
		toFavorites = (Button) findViewById(R.id.activity_contact_select_action_button_to_favorites);
		cancel = (Button) findViewById(R.id.activity_contact_select_action_button_cancel);

		change.setOnClickListener(changeClickListener);
		delete.setOnClickListener(deleteClickListener);
		toFavorites.setOnClickListener(toFavoritesClickListener);
		cancel.setOnClickListener(cancelClickListener);

		extras = getIntent().getExtras();
	}

	OnClickListener changeClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			startActivityForResult(new Intent(ContactSelectAction.this,
					EditContact.class).putExtras(getIntent().getExtras()), 1);
		}
	};

	OnClickListener deleteClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			DBOperations.deleteContact(ContactSelectAction.this,
					extras.getString(Constant.EXTRA_CONTACT_ID), extras);
			setResult(ResultCodes.CONTACT_SELECT_ACTION);
			finish();
		}
	};

	OnClickListener toFavoritesClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			if (DBOperations.addContactToFavorites(ContactSelectAction.this,
					extras.getString(Constant.EXTRA_CONTACT_NAME),
					extras.getString(Constant.EXTRA_CONTACT_SURNAME),
					extras.getString(Constant.EXTRA_CONTACT_PATRONYMIC),
					extras.getString(Constant.EXTRA_CONTACT_PHONE),
					extras.getString(Constant.EXTRA_CONTACT_CATEGORY),
					extras.getString(Constant.EXTRA_CONTACT_DATE_OF_BIRTH),
					extras.getString(Constant.EXTRA_CONTACT_PICTURE)) == DBOperations.DUPLICATE_DATA_ERROR_CODE) {
				setResult(
						ResultCodes.CONTACT_SELECT_ACTION_ACTION_TO_FAVORITES,
						new Intent().putExtra(Constant.EXTRA_ERROR_CODE,
								DBOperations.DUPLICATE_DATA_ERROR_CODE));
			}
			finish();
		}
	};

	OnClickListener cancelClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			finish();
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		setResult(ResultCodes.CONTACT_SELECT_ACTION);
	};

}
