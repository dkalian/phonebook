package com.soft.phonebook;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.constant.ResultCodes;
import com.soft.phonebook.database.DBOperations;
import com.soft.phonebook.design.BitmapUtils;
import com.soft.phonebook.design.Decoder;
import com.soft.phonebook.design.EditTextFormatted;

/**
 * Активность, для редактирования контакта
 * 
 * @author Nickolai
 * 
 */
public class EditContact extends Activity {

	EditText name, surname, patronymic, phone;
	EditTextFormatted dateOfBirth;
	ImageView picture;
	Spinner category;
	Button save, cancel;
	protected Intent data;

	String selectedPictureURI = "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_edit_contact);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.WHITE));

		data = getIntent();

		name = (EditText) findViewById(R.id.activity_edit_contact_name);
		surname = (EditText) findViewById(R.id.activity_edit_contact_surname);
		patronymic = (EditText) findViewById(R.id.activity_edit_contact_patronymic);
		phone = (EditText) findViewById(R.id.activity_edit_contact_phone);
		dateOfBirth = (EditTextFormatted) findViewById(R.id.activity_edit_contact_date_of_birth);
		dateOfBirth.setMask("dd.dd.dddd");

		picture = (ImageView) findViewById(R.id.activity_edit_contact_photo);

		picture.setOnClickListener(pictureClickListener);

		category = (Spinner) findViewById(R.id.activity_edit_contact_category);

		save = (Button) findViewById(R.id.activity_edit_contact_button_save);
		save.setOnClickListener(saveClickListener);
		cancel = (Button) findViewById(R.id.activity_edit_contact_button_cancel);
		restoreTypedData();

	}

	OnClickListener pictureClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"),
					ResultCodes.PICTURE_SELECTOR);

		}
	};

	OnClickListener saveClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View arg0) {
			if (isValidated()) {
				Bundle extras = data.getExtras();
				DBOperations.changeContact(EditContact.this, extras
						.getString(Constant.EXTRA_CONTACT_ID), name.getText()
						.toString(), surname.getText().toString(), patronymic
						.getText().toString(), phone.getText().toString(),
						category.getSelectedItem().toString(), dateOfBirth
								.getText().toString(), selectedPictureURI);
				Intent data = new Intent();

				data.putExtra(Constant.EXTRA_CONTACT_NAME, name.getText()
						.toString());
				data.putExtra(Constant.EXTRA_CONTACT_SURNAME, surname.getText()
						.toString());
				data.putExtra(Constant.EXTRA_CONTACT_PATRONYMIC, patronymic
						.getText().toString());
				data.putExtra(Constant.EXTRA_CONTACT_PHONE, phone.getText()
						.toString());
				data.putExtra(Constant.EXTRA_CONTACT_CATEGORY, category
						.getSelectedItem().toString());
				data.putExtra(Constant.EXTRA_CONTACT_DATE_OF_BIRTH, dateOfBirth
						.getText().toString());
				data.putExtra(Constant.EXTRA_CONTACT_PICTURE,
						selectedPictureURI);

				DBOperations.changeFavoriteContact(EditContact.this,
						extras.getString(Constant.EXTRA_CONTACT_NAME),
						extras.getString(Constant.EXTRA_CONTACT_SURNAME),
						extras.getString(Constant.EXTRA_CONTACT_PATRONYMIC),
						extras.getString(Constant.EXTRA_CONTACT_PHONE),
						extras.getString(Constant.EXTRA_CONTACT_CATEGORY),
						extras.getString(Constant.EXTRA_CONTACT_DATE_OF_BIRTH),
						extras.getString(Constant.EXTRA_CONTACT_PICTURE), data);
				setResult(ResultCodes.EDIT_CONTACT, data);
				finish();
			}
		}
	};

	/**
	 * Восстанавливает введенные данные пользователем.
	 */
	private void restoreTypedData() {
		if (data != null) {
			Bundle extras = data.getExtras();
			name.setText(extras.getString(Constant.EXTRA_CONTACT_NAME));
			surname.setText(extras.getString(Constant.EXTRA_CONTACT_SURNAME));
			patronymic.setText(extras
					.getString(Constant.EXTRA_CONTACT_PATRONYMIC));
			phone.setText(extras.getString(Constant.EXTRA_CONTACT_PHONE));
			dateOfBirth.setText(extras
					.getString(Constant.EXTRA_CONTACT_DATE_OF_BIRTH));

			ArrayList<String> categories = DBOperations.getCategoriesList(this);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					R.layout.spinner_item, R.id.TextView1,
					DBOperations.getCategoriesList(this));
			adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
			category.setPrompt("Выберите категорию");

			category.setAdapter(adapter);

			for (int i = 0; i < categories.size(); i++) {
				if (categories.get(i).compareTo(
						extras.getString(Constant.EXTRA_CONTACT_CATEGORY)) == 0) {
					category.setSelection(i);
				}
			}

			selectedPictureURI = data
					.getStringExtra(Constant.EXTRA_CONTACT_PICTURE);
			if (selectedPictureURI.length() != 0) {
				try {
					picture.setImageBitmap(BitmapUtils
							.getCircleMaskedBitmapUsingPorterDuff(
									Decoder.decodeUri(EditContact.this,
											Uri.parse(selectedPictureURI)), 500));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
								.decodeResource(
										EditContact.this.getResources(),
										R.drawable.no_image), 100));
			}
		}
	}

	/**
	 * Порверят введенные данные на корретность
	 * 
	 * @return true - введенные данные корретны
	 */
	boolean isValidated() {
		if (name.getText().toString().length() == 0) {
			name.setError("Укажите Имя");
			return false;
		}
		if (phone.getText().toString().length() == 0) {
			phone.setError("Укажите телефон");
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case ResultCodes.PICTURE_SELECTOR: {
			if (data != null) {
				String pictureURI = data.getDataString();
				selectedPictureURI = pictureURI;
				System.out.println(pictureURI);
				try {
					picture.setImageBitmap(BitmapUtils
							.getCircleMaskedBitmapUsingPorterDuff(
									Decoder.decodeUri(EditContact.this,
											Uri.parse(pictureURI)), 500));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			break;
		}
		default:
			break;
		}
	}

}
