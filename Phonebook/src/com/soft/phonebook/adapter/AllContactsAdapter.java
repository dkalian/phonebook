package com.soft.phonebook.adapter;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.phonebook.R;
import com.soft.phonebook.SendSMSActivity;
import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.database.DBConstant;
import com.soft.phonebook.database.DBOperations;
import com.soft.phonebook.design.BitmapUtils;
import com.soft.phonebook.design.Decoder;

/**
 * Адаптер для списка со всеми контактами
 * 
 * @author Nickolai
 * 
 */
public class AllContactsAdapter extends BaseAdapter implements Filterable {
	ArrayList<HashMap<String, String>> data;
	private Context context;

	/**
	 * Конструктор
	 * 
	 * @param context
	 *            Activity context
	 */
	public AllContactsAdapter(Context context) {
		data = DBOperations.getAllContacts(context);
		this.context = context;
	}

	/**
	 * Обновляет данные в адаптере
	 */
	public void updateDataSet() {
		data = DBOperations.getAllContacts(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return data.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public HashMap<String, String> getItem(int id) {
		return data.get(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int id) {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.item_contact, null);
		}

		ImageButton call = (ImageButton) view
				.findViewById(R.id.item_contact_button_call);
		call.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					// создаём новое Activity и делаем вызов
					context.startActivity(new Intent(
							"android.intent.action.CALL", Uri.parse("tel:"
									+ data.get(position).get(
											DBConstant.FIELD_PHONE))));
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});

		ImageButton message = (ImageButton) view
				.findViewById(R.id.item_contact_button_message);
		message.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent sendSMSActivity = new Intent(context,
						SendSMSActivity.class);
				sendSMSActivity.putExtra(Constant.EXTRA_CONTACT_PHONE, data
						.get(position).get(DBConstant.FIELD_PHONE));
				sendSMSActivity.putExtra(Constant.EXTRA_CONTACT_NAME,
						data.get(position).get(DBConstant.FIELD_NAME));
				sendSMSActivity.putExtra(Constant.EXTRA_CONTACT_SURNAME, data
						.get(position).get(DBConstant.FIELD_SURNAME));
				context.startActivity(sendSMSActivity);
			}
		});

		call.setFocusable(false);
		message.setFocusable(false);

		ImageView picture = (ImageView) view
				.findViewById(R.id.item_contact_picture);
		if (data.get(position).get(DBConstant.FIELD_PICTURE).length() == 0) {
			picture.setImageBitmap(BitmapUtils
					.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
							.decodeResource(context.getResources(),
									R.drawable.no_image), 40));
		} else {
			Uri uri = Uri.parse(data.get(position)
					.get(DBConstant.FIELD_PICTURE));
			try {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(
								Decoder.decodeUri(context, uri), 40));
			} catch (FileNotFoundException e) {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
								.decodeResource(context.getResources(),
										R.drawable.no_image), 40));
				e.printStackTrace();
			}
		}

		TextView nameAndSurname = (TextView) view
				.findViewById(R.id.item_contact_name_and_surname);
		nameAndSurname.setText(data.get(position).get(DBConstant.FIELD_NAME)
				+ " " + data.get(position).get(DBConstant.FIELD_SURNAME));
		return view;
	}

	/*
	 * Фильтр. (non-Javadoc)
	 * 
	 * @see android.widget.Filterable#getFilter()
	 */
	@SuppressLint("DefaultLocale")
	@Override
	public Filter getFilter() {

		return new Filter() {

			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
				data = (ArrayList<HashMap<String, String>>) filterResults.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				updateDataSet();
				List<HashMap<String, String>> filterResults = new ArrayList<HashMap<String, String>>();
				FilterResults results = new FilterResults();
				if (constraint != null && constraint.toString().length() > 0) {
					filterResults = FilterSearch(constraint, data);
					results.values = filterResults;
					results.count = filterResults.size();
				} else {
					updateDataSet();
					results.values = data;
					results.count = data.size();
				}
				return results;
			}

			@SuppressLint("DefaultLocale")
			private List<HashMap<String, String>> FilterSearch(
					CharSequence charSequence,
					List<HashMap<String, String>> rawData) {
				ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
				for (HashMap<String, String> data : rawData) {
					/*
					 * Если имя|фамилия|отчество|дата рождения начинаются с
					 * введенной строки то возвращаем элементы удавлетворяющие
					 * этому условию
					 */
					if (String.valueOf(data.get(DBConstant.FIELD_NAME))
							.toUpperCase()
							.startsWith(charSequence.toString().toUpperCase())
							|| String
									.valueOf(data.get(DBConstant.FIELD_SURNAME))
									.toUpperCase()
									.startsWith(
											charSequence.toString()
													.toUpperCase())
							|| String
									.valueOf(data.get(DBConstant.FIELD_PHONE))
									.toUpperCase()
									.startsWith(
											charSequence.toString()
													.toUpperCase())
							|| String
									.valueOf(
											data.get(DBConstant.FIELD_DATE_OF_BIRTH))
									.toUpperCase()
									.startsWith(
											charSequence.toString()
													.toUpperCase())
							|| String
									.valueOf(
											data.get(DBConstant.FIELD_PATRONYMIC))
									.toUpperCase()
									.startsWith(
											charSequence.toString()
													.toUpperCase())) {
						result.add(data);
					}
				}
				return result;
			}
		};
	}

}
