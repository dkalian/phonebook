package com.soft.phonebook.adapter;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.phonebook.R;
import com.soft.phonebook.SendSMSActivity;
import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.database.DBConstant;
import com.soft.phonebook.database.DBOperations;
import com.soft.phonebook.design.BitmapUtils;
import com.soft.phonebook.design.Decoder;

/**
 * Адаптер для списка с категориями
 * 
 * @author Nickolai
 * 
 */
public class CategoriesAdapter extends BaseExpandableListAdapter {
	ArrayList<String> categories;
	ArrayList<ArrayList<HashMap<String, String>>> contacts = new ArrayList<ArrayList<HashMap<String, String>>>();
	private Context context;

	/**
	 * Конструктор класса. Получает данные из БД и упаковывает двумерный список
	 * 
	 * @param context
	 */
	public CategoriesAdapter(Context context) {
		categories = DBOperations.getCategoriesList(context);
		// MAGIC! DO NOT TOUCH!
		for (int category = 0; category < categories.size(); category++) {
			contacts.add(DBOperations.getContactsListByCategory(context,
					categories.get(category)));
		}
		this.context = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChild(int, int)
	 */
	@Override
	public Object getChild(int groupPos, int childPos) {
		return contacts.get(groupPos).get(childPos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildId(int, int)
	 */
	@Override
	public long getChildId(int groupPos, int childPos) {
		return childPos;
	}

	/**
	 * Обновляет данные в адаптере
	 */
	public void updateDataSet() {
		categories = DBOperations.getCategoriesList(context);
		contacts = new ArrayList<ArrayList<HashMap<String, String>>>();
		for (int category = 0; category < categories.size(); category++) {
			contacts.add(DBOperations.getContactsListByCategory(context,
					categories.get(category)));
		}
	}

	/**
	 * Возвращает имя категории по позиции
	 * 
	 * @param pos
	 *            позиция
	 * @return Имя категории
	 */
	public String getCategoryNameByPosition(int pos) {
		return categories.get(pos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildView(int, int, boolean,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.item_contact, null);
		}

		ImageButton call = (ImageButton) view
				.findViewById(R.id.item_contact_button_call);
		call.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					// создаём новое Activity и делаем вызов
					context.startActivity(new Intent(
							"android.intent.action.CALL", Uri.parse("tel:"
									+ contacts.get(groupPosition)
											.get(childPosition)
											.get(DBConstant.FIELD_PHONE))));
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});

		ImageButton message = (ImageButton) view
				.findViewById(R.id.item_contact_button_message);
		message.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent sendSMSActivity = new Intent(context,
						SendSMSActivity.class);
				sendSMSActivity.putExtra(
						Constant.EXTRA_CONTACT_PHONE,
						contacts.get(groupPosition).get(childPosition)
								.get(DBConstant.FIELD_PHONE));
				sendSMSActivity.putExtra(
						Constant.EXTRA_CONTACT_NAME,
						contacts.get(groupPosition).get(childPosition)
								.get(DBConstant.FIELD_NAME));
				sendSMSActivity.putExtra(
						Constant.EXTRA_CONTACT_SURNAME,
						contacts.get(groupPosition).get(childPosition)
								.get(DBConstant.FIELD_SURNAME));
				context.startActivity(sendSMSActivity);
			}
		});
		call.setFocusable(false);
		message.setFocusable(false);

		ImageView picture = (ImageView) view
				.findViewById(R.id.item_contact_picture);
		if (contacts.get(groupPosition).get(childPosition)
				.get(DBConstant.FIELD_PICTURE).length() == 0) {
			picture.setImageBitmap(BitmapUtils
					.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
							.decodeResource(context.getResources(),
									R.drawable.no_image), 40));
		} else {
			Uri uri = Uri.parse(contacts.get(groupPosition).get(childPosition)
					.get(DBConstant.FIELD_PICTURE));
			try {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(
								Decoder.decodeUri(context, uri), 40));
			} catch (FileNotFoundException e) {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
								.decodeResource(context.getResources(),
										R.drawable.no_image), 40));
				e.printStackTrace();
			}
		}

		TextView nameAndSurname = (TextView) view
				.findViewById(R.id.item_contact_name_and_surname);
		nameAndSurname.setText(contacts.get(groupPosition).get(childPosition)
				.get(DBConstant.FIELD_NAME)
				+ " "
				+ contacts.get(groupPosition).get(childPosition)
						.get(DBConstant.FIELD_SURNAME));
		return view;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildrenCount(int)
	 */
	@Override
	public int getChildrenCount(int groupPos) {
		return contacts.get(groupPos).size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroup(int)
	 */
	@Override
	public Object getGroup(int pos) {
		return categories.get(pos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupCount()
	 */
	@Override
	public int getGroupCount() {
		return categories.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupId(int)
	 */
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupView(int, boolean,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.item_category, null);
		}

		TextView category = (TextView) view
				.findViewById(R.id.item_category_category_name);
		category.setText(categories.get(groupPosition)
				+ " ("
				+ DBOperations.getContactsListByCategory(context,
						categories.get(groupPosition)).size() + ")");
		return view;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#hasStableIds()
	 */
	@Override
	public boolean hasStableIds() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#isChildSelectable(int, int)
	 */
	@Override
	public boolean isChildSelectable(int groupPos, int childPos) {
		return true;
	}
}
