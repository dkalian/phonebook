package com.soft.phonebook.adapter;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.phonebook.R;
import com.soft.phonebook.SendSMSActivity;
import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.database.DBConstant;
import com.soft.phonebook.database.DBOperations;
import com.soft.phonebook.design.BitmapUtils;
import com.soft.phonebook.design.Decoder;

/**
 * Адаптер для списка с избранными контактами
 * 
 * @author Nickolai
 * 
 */
public class FavoritesAdapter extends BaseAdapter {
	ArrayList<HashMap<String, String>> data;
	private Context context;

	/**
	 * Констуктор
	 * 
	 * @param context
	 *            Activity context
	 */
	public FavoritesAdapter(Context context) {
		this.context = context;
		data = DBOperations.getAllFavorites(context);
	}

	/**
	 * Обновляет данные адаптере
	 */
	public void updateDataSet() {
		data = DBOperations.getAllFavorites(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public HashMap<String, String> getItem(int id) {
		// TODO Auto-generated method stub
		return data.get(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int id) {
		// TODO Auto-generated method stub
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.item_contact, null);
		}

		ImageButton call = (ImageButton) view
				.findViewById(R.id.item_contact_button_call);
		call.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					// создаём новое Activity и делаем вызов
					context.startActivity(new Intent(
							"android.intent.action.CALL", Uri.parse("tel:"
									+ data.get(position).get(
											DBConstant.FIELD_PHONE))));
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});

		ImageButton message = (ImageButton) view
				.findViewById(R.id.item_contact_button_message);
		message.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent sendSMSActivity = new Intent(context,
						SendSMSActivity.class);
				sendSMSActivity.putExtra(Constant.EXTRA_CONTACT_PHONE, data
						.get(position).get(DBConstant.FIELD_PHONE));
				sendSMSActivity.putExtra(Constant.EXTRA_CONTACT_NAME,
						data.get(position).get(DBConstant.FIELD_NAME));
				sendSMSActivity.putExtra(Constant.EXTRA_CONTACT_SURNAME, data
						.get(position).get(DBConstant.FIELD_SURNAME));
				context.startActivity(sendSMSActivity);
			}
		});
		call.setFocusable(false);
		message.setFocusable(false);

		ImageView picture = (ImageView) view
				.findViewById(R.id.item_contact_picture);
		if (data.get(position).get(DBConstant.FIELD_PICTURE).length() == 0) {
			picture.setImageBitmap(BitmapUtils
					.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
							.decodeResource(context.getResources(),
									R.drawable.no_image), 40));
		} else {
			Uri uri = Uri.parse(data.get(position)
					.get(DBConstant.FIELD_PICTURE));
			try {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(
								Decoder.decodeUri(context, uri), 40));
			} catch (FileNotFoundException e) {
				picture.setImageBitmap(BitmapUtils
						.getCircleMaskedBitmapUsingPorterDuff(BitmapFactory
								.decodeResource(context.getResources(),
										R.drawable.no_image), 40));
				e.printStackTrace();
			}
		}

		TextView nameAndSurname = (TextView) view
				.findViewById(R.id.item_contact_name_and_surname);
		nameAndSurname.setText(data.get(position).get(DBConstant.FIELD_NAME)
				+ " " + data.get(position).get(DBConstant.FIELD_SURNAME));
		return view;
	}
}
