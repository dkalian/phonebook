package com.soft.phonebook.design;

import java.util.regex.Pattern;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created with IntelliJ IDEA.
 * User: alexkovalev
 * Date: 11.04.12
 * Time: 5:36
 */
public class EditTextFormatted extends EditText implements TextWatcher {

    private static final String CHANGED_CHAR_IN_MASK = "daAxXz"; // символы в маске на месте вводимых значений

    private String mask;
    private StringBuilder textWithoutMask;
    private StringBuilder textWithMask;
    private boolean isTextChange;
    private boolean isTextSelected;
    private int maxLengthTextWithoutMask;
    private int cursorPosition;
    private static Pattern numberPattern = Pattern.compile("[0-9]");
    private static Pattern lettersPattern = Pattern.compile("[a-zA-Z]");
    private static Pattern lettersAndNumberPattern = Pattern.compile("[a-zA-Z0-9]");
    private static Pattern lettersAndWildcardsPattern = Pattern.compile("[a-zA-Z[[:punct:]]]");
    private static Pattern lettersNumberWildcardsPattern = Pattern.compile("[a-zA-Z0-9[[:punct:]]]");

    public EditTextFormatted(Context context) {
        super(context);
        init();
    }

    public EditTextFormatted(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextFormatted(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mask = null;
        cursorPosition = 0;
        isTextChange = false;
        isTextSelected = false;
        textWithoutMask = new StringBuilder();
        textWithMask = new StringBuilder();
        this.addTextChangedListener(this);
    }

    public String getMask() {
        return mask;
    }

    private void setTextSelected(boolean b) {
        isTextSelected = b;
    }

    public void setMask(String mask) {
        this.mask = mask;
        maxLengthTextWithoutMask = 0;
        textWithoutMask.setLength(0);
        for (int i = 0; i < mask.length(); i++) {
            if (isPositionForInput(i)) {
                maxLengthTextWithoutMask++;
            }
        }
        isTextChange = true;
        setText(getTextWithMask());
        setSelection(getPositionCursor(0));
    }

    public String getTextWithoutMask() {
        return textWithoutMask.toString();
    }

    private String getTextWithMask() {
        int indexTextWithoutMask = 0;
        textWithMask.delete(0, textWithMask.length());
        for (int i = 0; i < mask.length(); i++) {
            if (isPositionForInput(i)) {
                if (textWithoutMask.length() > indexTextWithoutMask) {
                    textWithMask.append(textWithoutMask.charAt(indexTextWithoutMask));
                    indexTextWithoutMask++;
                } else {
                    textWithMask.append('_');
                }

            } else {
                textWithMask.append(mask.charAt(i));
            }
        }
        return textWithMask.toString();
    }

    private int getPositionCursor(int positionInTextWithoutMask) {
        int position = 0;
        int i;
        for (i = 0; i < mask.length(); i++) {
            if (isPositionForInput(i)) {
                if (positionInTextWithoutMask == position) {
                    return i;
                } else {
                    position++;
                }
            }
        }
        return i;
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (mask == null) return;
        if (getSelectionEnd() - getSelectionStart() != 0) {
            setTextSelected(true);
        }
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (mask == null) return;
        if (s.length() <= 0 && before <= 0 && count <= 0) {
            return;
        }
        if (isTextChange || textWithMask.toString().equals(s.toString())) {           // Если текст меняли мы, то ничего не делаем
            isTextChange = false;
            return;
        }


        if (before > 0) {
            deleteInTextWithoutMask(start, before);
        }

        if (count > 0) {
            insertInTextWithoutMask(start, count);
        }

        if (textWithoutMask.length() > maxLengthTextWithoutMask) {
            textWithoutMask.setLength(maxLengthTextWithoutMask);
        }


        setText(getTextWithMask());
        isTextChange = true;
        setSelection(getPositionCursor(cursorPosition));
    }

    protected void onSelectionChanged(int selStart, int selEnd) {
        if (mask == null) return;
        if (textWithoutMask != null || getText().length() > 1) {
            int maxPosition = getPositionCursor(textWithoutMask.length());
            if (selStart > maxPosition || selEnd > maxPosition) {
                if (selStart > maxPosition) {
                    selStart = maxPosition;
                }
                if (selEnd > maxPosition) {
                    selEnd = maxPosition;
                }
                setSelection(selStart, selEnd);
            }
            if (selEnd != selStart) {
                setTextSelected(true);
            }
        }
    }

    // проверка, является ли вводимым символ с индексом i в маске
    private boolean isPositionForInput(int i) {
        return CHANGED_CHAR_IN_MASK.indexOf(mask.charAt(i)) >= 0;
    }

    private void deleteInTextWithoutMask(int start, int count) {
        int indexDeleteStart = 0;
        int indexDeleteStop = 0;
        for (int i = 0; i < start + count; i++) {
            if (isPositionForInput(i)) {
                if (i < start) {
                    indexDeleteStart++;
                }
                indexDeleteStop++;
            }
        }
        if (isTextSelected == false && !isPositionForInput(start) && start > 0
                && count <= 1 && indexDeleteStart > 0) {
            indexDeleteStart--;
        }
        if (indexDeleteStop > textWithoutMask.length()) {
            indexDeleteStop = textWithoutMask.length();
        }
        if (indexDeleteStop > indexDeleteStart) {
            textWithoutMask.delete(indexDeleteStart, indexDeleteStop);
        }

        cursorPosition = indexDeleteStart;
        setTextSelected(false);
    }

    private void insertInTextWithoutMask(int start, int count) {
        StringBuilder addedText = new StringBuilder(getText().subSequence(start, start + count));
        // если номер телефона
        cutUnnessesarySymbols(addedText);

        int indexInTextWithoutMask = 0;
        for (int i = 0; i < start && i < mask.length()
                && indexInTextWithoutMask < textWithoutMask.length(); i++) {
            if (isPositionForInput(i)) {
                indexInTextWithoutMask++;
            }
        }
        cursorPosition = indexInTextWithoutMask;

        for (int i = 0; i < addedText.length() && textWithoutMask.length() < maxLengthTextWithoutMask; i++) {
            if (isCorrectCharForThisPosition(getPositionCursor(indexInTextWithoutMask), addedText.charAt(i))) {
                textWithoutMask.insert(indexInTextWithoutMask, addedText.charAt(i));
                indexInTextWithoutMask++;
                cursorPosition++;
            }
        }
    }

    private void cutUnnessesarySymbols(StringBuilder addedText) {
        if (mask.startsWith("+7") && textWithoutMask.length() == 0) {
            if (addedText.length() == 11 && addedText.charAt(0) == '8') {
                addedText.delete(0, 1);
            }
            if (addedText.length() == 11 && addedText.charAt(0) == '7') {
                addedText.delete(0, 1);
            }
            if (addedText.length() == 12 && addedText.toString().startsWith("+7")) {
                addedText.delete(0, 2);
            }
        }
    }

    private boolean isCorrectCharForThisPosition(int index, char inputChar) {
        switch (mask.charAt(index)) {
            case 'd': {
                return numberPattern.matcher(Character.toString(inputChar)).matches();
            }
            case 'a': {
                return lettersPattern.matcher(Character.toString(inputChar)).matches();
            }
            case 'A': {
                return lettersAndNumberPattern.matcher(Character.toString(inputChar)).matches();
            }
            case 'x': {
                return lettersAndWildcardsPattern.matcher(Character.toString(inputChar)).matches();
            }
            case 'X': {
                return lettersNumberWildcardsPattern.matcher(Character.toString(inputChar)).matches();
            }
            default: {
                return true;
            }
        }
    }

    public String getOriginalValue() {
        return getTextWithoutMask();
    }
}
