package com.soft.phonebook.design;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.soft.phonebook.R;

/**
 * Класс с изменненными тостами
 * 
 * @author Nickolai
 * 
 */
public class CustomToast {
	/**
	 * Получение "успешного" тоста
	 * 
	 * @param context
	 *            контекст
	 * @param message
	 *            сообщение
	 * @param duration
	 *            задержка
	 * @return Toast
	 */
	@SuppressWarnings("deprecation")
	public static Toast getSuccessToast(Context context, String message,
			int duration) {
		Toast toast = Toast.makeText(context, message, duration);
		View view = toast.getView();
		view.setBackgroundDrawable(context.getResources().getDrawable(
				R.drawable.green_toast));
		return toast;
	}

	/**
	 * Получение "ошибочного" тоста
	 * @param context контекст
	 * @param message сообщение
	 * @param duration задержка
	 * @return Toast
	 */
	@SuppressWarnings("deprecation")
	public static Toast getErrorToast(Context context, String message,
			int duration) {
		Toast toast = Toast.makeText(context, message, duration);
		View view = toast.getView();
		view.setBackgroundDrawable(context.getResources().getDrawable(
				R.drawable.red_toast));
		return toast;
	}

	/**
	 * Получение "информационного" тоста
	 * 
	 * @param context
	 *            контекст
	 * @param message
	 *            сообщение
	 * @param duration
	 *            задержка
	 * @return Toast
	 */
	@SuppressWarnings("deprecation")
	
	public static Toast getInfoToast(Context context, String message,
			int duration) {
		Toast toast = Toast.makeText(context, message, duration);
		View view = toast.getView();
		view.setBackgroundDrawable(context.getResources().getDrawable(
				R.drawable.blue_toast));
		return toast;
	}
}
