package com.soft.phonebook.design;

import java.io.FileNotFoundException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

/**
 * Класс для дектодировки Bitmap`a по URI
 * @author Nickolai
 *
 */
public class Decoder {
	
	/**
	 * Декодировать
	 * @param context контекст
	 * @param selectedImage URI выбранной картинки
	 * @return Bitmap
	 * @throws FileNotFoundException файл не найден
	 */
	public static Bitmap decodeUri(Context context ,Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
        		context.getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
        		context.getContentResolver().openInputStream(selectedImage), null, o2);
    }
}
