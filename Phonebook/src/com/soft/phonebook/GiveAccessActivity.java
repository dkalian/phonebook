package com.soft.phonebook;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.soft.phonebook.constant.Constant;

/**
 * Активность с вводом главного пароля
 * 
 * @author Nickolai
 * 
 */
public class GiveAccessActivity extends ActionBarActivity {

	Button accept;
	EditText password;
	String pass;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_give_access);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.phonebookstyle_color));
		accept = (Button) findViewById(R.id.activity_give_access_accept);
		accept.setOnClickListener(acceptClickListener);

		password = (EditText) findViewById(R.id.activity_give_access_password);

		updateDataSet();
		System.out.println("On Create GAA");
	}

	/**
	 * Получение сохранненого пароля
	 */
	void updateDataSet() {
		System.out.println("Update Data Set");
		SharedPreferences customSharedPreference = getSharedPreferences(
				Constant.SHARED_PREFERENCES_NAME, Activity.MODE_APPEND);

		pass = customSharedPreference.getString(Constant.PREFERENCE_PASS, null);

		if (pass == null) {
			startActivityForResult(new Intent(GiveAccessActivity.this,
					ChangePasswordPreference.class), 1);
		}
	}

	OnClickListener acceptClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View arg0) {
			checkPassword();
		}
	};

	/**
	 * Проверка введенного пароля
	 */
	void checkPassword() {
		if (password.getText().toString().length() != 0) {
			if (pass != null
					&& pass.compareTo(password.getText().toString()) == 0) {
				startActivityForResult(new Intent(GiveAccessActivity.this,
						MainActivity.class), 1);
			} else {
				password.setError("Неверный пароль");
			}
			password.getText().clear();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.give_access, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case RESULT_OK:
			updateDataSet();
			System.out.println("On Actuvuty Result pass = " + pass);
			break;
		case RESULT_CANCELED:
			finish();
			break;
		default:
			break;
		}
	}

}
