package com.soft.phonebook;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.design.CustomToast;

/**
 * Активность для отправки смс
 * 
 * @author Nickolai
 * 
 */
public class SendSMSActivity extends Activity {
	Button sendSMS, cancel;

	EditText messageText;
	TextView recipient;
	Bundle extras;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_send_sms);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.WHITE));

		sendSMS = (Button) findViewById(R.id.activity_send_sms_button_send);
		cancel = (Button) findViewById(R.id.activity_send_sms_cancel);

		sendSMS.setOnClickListener(sendSMSClickListener);
		cancel.setOnClickListener(cancelClickListener);

		recipient = (TextView) findViewById(R.id.activity_send_sms_sender_name);
		messageText = (EditText) findViewById(R.id.activity_send_sms_text_message);

		extras = getIntent().getExtras();
		recipient.setText(extras.getString(Constant.EXTRA_CONTACT_NAME) + " "
				+ extras.getString(Constant.EXTRA_CONTACT_SURNAME));
	}

	OnClickListener sendSMSClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			// Создаем смс менеджер
			SmsManager smsManager = SmsManager.getDefault();
			// устанавливаем текст сообщения и отправляем
			smsManager.sendTextMessage(
					extras.getString(Constant.EXTRA_CONTACT_PHONE), null,
					messageText.getText().toString(), null, null);
			CustomToast.getInfoToast(SendSMSActivity.this,
					"Сообщение отправлено", Toast.LENGTH_LONG).show();
			finish();
		}
	};

	OnClickListener cancelClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			finish();
		}
	};

}
