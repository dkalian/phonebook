package com.soft.phonebook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.constant.ResultCodes;

/**
 * Активность, создающая категорию
 * 
 * @author Nickolai
 * 
 */
public class CreateCategory extends Activity {

	Button save, cancel;
	TextView categoryName;
	String oldCategoryName;

	/**
	 * Проверяет, корректны ли введенные данные
	 * 
	 * @return true - данные корректны
	 */
	boolean isValidated() {
		if (categoryName.getText().toString().length() == 0) {
			categoryName.setError("Заполните это поле");
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_create_category);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.WHITE));

		save = (Button) findViewById(R.id.activity_create_category_button_save);
		cancel = (Button) findViewById(R.id.activity_create_category_button_cancel);

		save.setOnClickListener(saveClickListener);
		cancel.setOnClickListener(cancelClickListener);

		categoryName = (TextView) findViewById(R.id.activity_create_category_category_name);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			oldCategoryName = getIntent().getExtras().getString(
					Constant.EXTRA_SELECTED_CATEGORY_NAME);
			if (oldCategoryName != null) {
				categoryName.setText(oldCategoryName);
			}
		}

	}

	OnClickListener saveClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			if (isValidated()) {
				Intent data = new Intent();
				data.putExtra(Constant.EXTRA_NEW_CATEGORY_NAME, categoryName
						.getText().toString());
				setResult(ResultCodes.CREATE_CATEGORY, data);
				finish();
			}
		}
	};

	OnClickListener cancelClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			finish();
		}
	};
}
