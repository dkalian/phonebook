package com.soft.phonebook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.constant.ResultCodes;
import com.soft.phonebook.database.DBOperations;

/**
 * Активность, отображающая контекстное меню для категорий 
 * @author Nickolai
 * 
 */
public class CategorySelectAction extends Activity {

	Button change, delete, cancel;
	String categoryName;

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_category_select_action);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		categoryName = getIntent().getExtras().getString(
				Constant.EXTRA_SELECTED_CATEGORY_NAME);

		change = (Button) findViewById(R.id.activity_category_select_action_button_change);
		delete = (Button) findViewById(R.id.activity_category_select_action_button_delete);
		cancel = (Button) findViewById(R.id.activity_category_select_action_button_cancel);

		change.setOnClickListener(changeClickListener);
		delete.setOnClickListener(deleteClickListener);
		cancel.setOnClickListener(cancelClickListener);
	}

	OnClickListener changeClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			startActivityForResult(new Intent(CategorySelectAction.this,
					CreateCategory.class).putExtra(
					Constant.EXTRA_SELECTED_CATEGORY_NAME, categoryName), 1);
		}
	};

	OnClickListener deleteClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			DBOperations
					.deleteCategory(CategorySelectAction.this, categoryName);
			setResult(ResultCodes.CATEGORY_ACTION_DELETE);
			finish();
		}
	};

	OnClickListener cancelClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

		}
	};

	/* 
	 * (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		// Результат от активновсти создающей категории
		case ResultCodes.CREATE_CATEGORY: {
			// Забираем новое имя категории
			String newCategoryName = data.getExtras().getString(
					Constant.EXTRA_NEW_CATEGORY_NAME);
			// Добавляем новоую категорию
			DBOperations.changeCategory(this, categoryName, newCategoryName);
			// Возвращаем результат
			setResult(ResultCodes.CATEGORY_ACTION_DELETE);
			finish();
			break;
		}
		default:
			break;
		}
	}

}
