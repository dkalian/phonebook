package com.soft.phonebook;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.design.CustomToast;

/**
 * Активность, меняющая пароль
 * @author Nickolai
 * 
 */
public class ChangePasswordPreference extends ActionBarActivity {

	Button save, cancel;
	EditText typePassword, repeatPassword;
	CheckBox showPassword;

	/*
	 * (non-Javadoc)
	 * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password_preference);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.phonebookstyle_color));
		
		save = (Button) findViewById(R.id.activity_change_password_button_save);
		cancel = (Button) findViewById(R.id.activity_change_password_button_cancel);
		cancel.setOnClickListener(cancelClickListener);

		save.setOnClickListener(saveClickListener);

		typePassword = (EditText) findViewById(R.id.activity_change_password_type_password);
		repeatPassword = (EditText) findViewById(R.id.activity_change_password_repeat_password);

		showPassword = (CheckBox) findViewById(R.id.activity_change_password_show_password);
		showPassword.setOnCheckedChangeListener(changeListener);
		
		passwordInputType = typePassword.getInputType();
	}

	OnClickListener saveClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View arg0) {
			// Если пароль удовллетвряет условиям
			if (validatePassword()) {
				// То перезаписываем старый пароль новым
				SharedPreferences customSharedPreference = getSharedPreferences(
						Constant.SHARED_PREFERENCES_NAME, Activity.MODE_APPEND);
				SharedPreferences.Editor editor = customSharedPreference.edit();
				editor.putString(Constant.PREFERENCE_PASS, typePassword
						.getText().toString());
				editor.commit();
				// Оповещаем, что смена пароля успешно произведена и отправляем новый пароль на
				setResult(RESULT_OK, new Intent().putExtra(Constant.EXTRA_PASSWORD, typePassword.getText().toString()));
				CustomToast.getInfoToast(ChangePasswordPreference.this, "Пароль сохранен", Toast.LENGTH_LONG).show();
				finish();
			}
		}

		/**
		 * Проверка пароля на корректность
		 * Пароль корректен тогда, когда он не пустой,
		 * и когда оба поля собвпадают
		 * @return true - пароль корректен
		 */
		private boolean validatePassword() {
			if ((typePassword.length() == 0)) {
				typePassword.setError("Заполните это поле");
				return false;
			}
			if ((repeatPassword.length() == 0)) {
				repeatPassword.setError("Заполните это поле");
				return false;
			}

			if (repeatPassword.getText().toString()
					.compareTo(typePassword.getText().toString()) != 0) {
				repeatPassword.setError("Пароли не совпадают");
				return false;
			}
			return true;
		}
	};
	
	int passwordInputType;
	OnCheckedChangeListener changeListener = new OnCheckedChangeListener() {
		@Override
		/*
		 * Если галочка "Показать пароль" установлена, то отобразятся оба поля
		 * (non-Javadoc)
		 * @see android.widget.CompoundButton.OnCheckedChangeListener#onCheckedChanged(android.widget.CompoundButton, boolean)
		 */
		public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
			if (isChecked) { 
				typePassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
				repeatPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
			} else {
				typePassword.setInputType(passwordInputType);
				repeatPassword.setInputType(passwordInputType);
			}
		}
	};

	OnClickListener cancelClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View arg0) {
			// Уведомляем, вызвавшую активность, что действие отменено
			setResult(RESULT_CANCELED);
			finish();
		}
	};

}
