package com.soft.phonebook;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;

import com.soft.phonebook.fragment.adapter.MainActivityPageAdapter;

/**
 * Главная активность с фрагментами и Page View`ером
 * 
 * @author Nickolai
 * 
 */
public class MainActivity extends ActionBarActivity {

	final String ACTIVITY_TITLE = "Phonebook";

	ViewPager pager;
	ActionBar actionBar;
	MainActivityPageAdapter mainActivityPageAdapter;
	int lastSelectedPage;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initViewPager();
		initActionBar();
	}

	/**
	 * Инициализация Action Bar`а
	 * 
	 * @return ActionBa
	 */
	private android.support.v7.app.ActionBar initActionBar() {
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.phonebookstyle_color));
		return actionBar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem settings = menu.findItem(R.id.main_menu_settings);
		settings.setOnMenuItemClickListener(itemClickListener);

		return true;
	}

	OnMenuItemClickListener itemClickListener = new OnMenuItemClickListener() {
		/*
		 * (non-Javadoc)
		 * @see android.view.MenuItem.OnMenuItemClickListener#onMenuItemClick(android.view.MenuItem)
		 */
		@Override
		public boolean onMenuItemClick(MenuItem arg0) {
			startActivity(new Intent(MainActivity.this, Preference.class));
			return false;
		}
	};

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	/**
	 * Инициализация Page View`ера
	 */
	void initViewPager() {
		pager = (ViewPager) findViewById(R.id.pager);
		mainActivityPageAdapter = new MainActivityPageAdapter(
				getSupportFragmentManager());
		pager.setAdapter(mainActivityPageAdapter);

		PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.pagerTabStrip);
		pagerTabStrip.setDrawFullUnderline(true);
		pagerTabStrip.setTextColor(getResources().getColor(
				R.color.phonebookstyle_color));
		pagerTabStrip.setTabIndicatorColor(getResources().getColor(
				R.color.phonebookstyle_color));

		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				lastSelectedPage = position;
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				//
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				//
			}

		});
		pager.setCurrentItem(1);
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stu
		super.onActivityResult(requestCode, resultCode, data);
	}

}
