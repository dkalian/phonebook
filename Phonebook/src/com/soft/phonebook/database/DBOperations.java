package com.soft.phonebook.database;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;

import com.soft.phonebook.constant.Constant;

/**
 * Различные операции с базрой данных
 * 
 * @author Nickolai
 * 
 */
public class DBOperations {

	private static final String LOG_TAG = "DATA BASE OPERATIONS";

	/**
	 * Добавление нового контакта в базу данных
	 * 
	 * @param context
	 *            контекст
	 * @param name
	 *            имя
	 * @param surname
	 *            фамилия
	 * @param patronymic
	 *            отчество
	 * @param phone
	 *            телефон
	 * @param category
	 *            категория
	 * @param dateOfBirth
	 *            дата рождения
	 * @param picture
	 *            URI изображения
	 * @return true - успешно добавлено
	 */
	// *****************CONSTACTS*********************//

	public static boolean addContact(Context context, String name,
			String surname, String patronymic, String phone, String category,
			String dateOfBirth, String picture) {
		boolean isSuccess = true;
		ContentValues cv = new ContentValues();
		cv.put(DBConstant.FIELD_NAME, name);
		cv.put(DBConstant.FIELD_SURNAME, surname);
		cv.put(DBConstant.FIELD_PATRONYMIC, patronymic);
		cv.put(DBConstant.FIELD_PHONE, phone);
		cv.put(DBConstant.FIELD_CATEGORY, category);
		cv.put(DBConstant.FIELD_DATE_OF_BIRTH, dateOfBirth);
		cv.put(DBConstant.FIELD_PICTURE, picture);

		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		try {
			long id = db
					.insertOrThrow(DBConstant.TABLE_NAME_CONTACTS, null, cv);
			Log.i(LOG_TAG, "Запись успешно добавлена. ID - " + id);
		} catch (SQLiteException e) {
			isSuccess = false;
			Log.e(LOG_TAG, "Ошибка при добавлении записи. STACRTRACE:");
			e.printStackTrace();
		}
		db.close();
		return isSuccess;
	}

	/**
	 * Получает список всех контактов
	 * 
	 * @param context
	 *            контекст
	 * @return ArrayList<HashMap<String, String>> контакты
	 */
	public static ArrayList<HashMap<String, String>> getAllContacts(
			Context context) {
		ArrayList<HashMap<String, String>> contacts = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		Cursor c = db.query(DBConstant.TABLE_NAME_CONTACTS, null, null, null,
				null, null, null, null);

		if (c.moveToFirst()) {
			do {

				int fId = c.getColumnIndex(DBConstant.FIELD_ID);
				int fName = c.getColumnIndex(DBConstant.FIELD_NAME);
				int fSurname = c.getColumnIndex(DBConstant.FIELD_SURNAME);
				int fPatronymic = c.getColumnIndex(DBConstant.FIELD_PATRONYMIC);
				int fDateOfBirth = c
						.getColumnIndex(DBConstant.FIELD_DATE_OF_BIRTH);
				int fPicture = c.getColumnIndex(DBConstant.FIELD_PICTURE);
				int fPhone = c.getColumnIndex(DBConstant.FIELD_PHONE);
				int fCategory = c.getColumnIndex(DBConstant.FIELD_CATEGORY);

				String id = c.getString(fId);
				String name = c.getString(fName);
				String surname = c.getString(fSurname);
				String patronymic = c.getString(fPatronymic);
				String dateOfBirth = c.getString(fDateOfBirth);
				String picture = c.getString(fPicture);
				String phone = c.getString(fPhone);
				String category = c.getString(fCategory);

				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put(DBConstant.FIELD_ID, id);
				temp.put(DBConstant.FIELD_NAME, name);
				temp.put(DBConstant.FIELD_SURNAME, surname);
				temp.put(DBConstant.FIELD_PATRONYMIC, patronymic);
				temp.put(DBConstant.FIELD_DATE_OF_BIRTH, dateOfBirth);
				temp.put(DBConstant.FIELD_PICTURE, picture);
				temp.put(DBConstant.FIELD_PHONE, phone);
				temp.put(DBConstant.FIELD_CATEGORY, category);

				contacts.add(temp);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return contacts;
	}

	/**
	 * Список контактов с определенной категорией
	 * 
	 * @param context
	 *            контекст
	 * @param categoryName
	 *            название категории
	 * @return ArrayList<HashMap<String, String>>
	 */
	public static ArrayList<HashMap<String, String>> getContactsListByCategory(
			Context context, String categoryName) {
		ArrayList<HashMap<String, String>> contacts = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		Cursor c = db.query(DBConstant.TABLE_NAME_CONTACTS, null,
				"`category` = '" + categoryName + "'", null, null, null, null,
				null);

		if (c.moveToFirst()) {
			do {

				int fId = c.getColumnIndex(DBConstant.FIELD_ID);
				int fName = c.getColumnIndex(DBConstant.FIELD_NAME);
				int fSurname = c.getColumnIndex(DBConstant.FIELD_SURNAME);
				int fPatronymic = c.getColumnIndex(DBConstant.FIELD_PATRONYMIC);
				int fDateOfBirth = c
						.getColumnIndex(DBConstant.FIELD_DATE_OF_BIRTH);
				int fPicture = c.getColumnIndex(DBConstant.FIELD_PICTURE);
				int fPhone = c.getColumnIndex(DBConstant.FIELD_PHONE);
				int fCategory = c.getColumnIndex(DBConstant.FIELD_CATEGORY);

				String id = c.getString(fId);
				String name = c.getString(fName);
				String surname = c.getString(fSurname);
				String patronymic = c.getString(fPatronymic);
				String dateOfBirth = c.getString(fDateOfBirth);
				String picture = c.getString(fPicture);
				String phone = c.getString(fPhone);
				String category = c.getString(fCategory);

				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put(DBConstant.FIELD_ID, id);
				temp.put(DBConstant.FIELD_NAME, name);
				temp.put(DBConstant.FIELD_SURNAME, surname);
				temp.put(DBConstant.FIELD_PATRONYMIC, patronymic);
				temp.put(DBConstant.FIELD_DATE_OF_BIRTH, dateOfBirth);
				temp.put(DBConstant.FIELD_PICTURE, picture);
				temp.put(DBConstant.FIELD_PHONE, phone);
				temp.put(DBConstant.FIELD_CATEGORY, category);

				contacts.add(temp);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return contacts;
	}

	/**
	 * обновляет данные в определенном контакте
	 * 
	 * @param context
	 *            контекст
	 * @param id
	 *            ИД обновляемого контакта
	 * @param name
	 *            имя
	 * @param surname
	 *            фамилия
	 * @param patronymic
	 *            отчество
	 * @param phone
	 *            телефон
	 * @param category
	 *            категория
	 * @param dateOfBirth
	 *            дата рождения
	 * @param picture
	 *            URI изображения
	 */
	public static void changeContact(Context context, String id, String name,
			String surname, String patronymic, String phone, String category,
			String dateOfBirth, String picture) {

		ContentValues cv = new ContentValues();
		cv.put(DBConstant.FIELD_NAME, name);
		cv.put(DBConstant.FIELD_SURNAME, surname);
		cv.put(DBConstant.FIELD_PATRONYMIC, patronymic);
		cv.put(DBConstant.FIELD_PHONE, phone);
		cv.put(DBConstant.FIELD_CATEGORY, category);
		cv.put(DBConstant.FIELD_DATE_OF_BIRTH, dateOfBirth);
		cv.put(DBConstant.FIELD_PICTURE, picture);

		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		try {
			int count = db.update(DBConstant.TABLE_NAME_CONTACTS, cv,
					DBConstant.FIELD_ID + " = '" + id + "'", null);
			Log.i(LOG_TAG, "Запись успешно обновлена. ID - " + id);
			Log.i(LOG_TAG, "Обновлено - " + count);
		} catch (SQLiteException e) {
			Log.e(LOG_TAG, "Ошибка при добавлении записи. STACRTRACE:");
			e.printStackTrace();
		}
		db.close();
	}

	/**
	 * Удаляет контакт
	 * 
	 * @param context
	 *            контекст
	 * @param id
	 *            ИД удаляемого контакта
	 * @param extras
	 *            данные удаляемого контакта
	 */
	public static void deleteContact(Context context, String id, Bundle extras) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		try {
			db.delete(DBConstant.TABLE_NAME_CONTACTS, DBConstant.FIELD_ID
					+ " = '" + id + "'", null); // Удаляем и из контактов
			// и из избранного
			deleteFromFavoritesContact(context,
					extras.getString(Constant.EXTRA_CONTACT_NAME),
					extras.getString(Constant.EXTRA_CONTACT_SURNAME),
					extras.getString(Constant.EXTRA_CONTACT_PATRONYMIC),
					extras.getString(Constant.EXTRA_CONTACT_PHONE),
					extras.getString(Constant.EXTRA_CONTACT_CATEGORY),
					extras.getString(Constant.EXTRA_CONTACT_DATE_OF_BIRTH),
					extras.getString(Constant.EXTRA_CONTACT_PICTURE));
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		db.close();
	}

	/**
	 * Удаление категории
	 * 
	 * @param context
	 *            контекст
	 * @param categoryName
	 *            имя категории
	 * @return int - количество записей, затронутых удалением категории
	 */
	// *****************CONSTACTS*********************//

	// *****************CATEGORIES*********************//

	public static int deleteCategory(Context context, String categoryName) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		int deleted = db.delete(DBConstant.TABLE_NAME_CATEGORIES,
				DBConstant.FIELD_CATEGORY + " = '" + categoryName + "'", null);
		System.out.println("DELETED RECORDS: " + deleted);
		ContentValues cv = new ContentValues();
		cv.put(DBConstant.FIELD_CATEGORY, "Без категории");
		int updatedRecordsInContacts = db.update(
				DBConstant.TABLE_NAME_CONTACTS, cv, DBConstant.FIELD_CATEGORY
						+ " = '" + categoryName + "'", null);
		db.close();
		return updatedRecordsInContacts;
	}

	/**
	 * Изменение/ переименовывание категории
	 * 
	 * @param context
	 *            контекст
	 * @param oldCategoryName
	 *            старое название категории
	 * @param newCategoryName
	 *            новое название категории
	 */
	public static void changeCategory(Context context, String oldCategoryName,
			String newCategoryName) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(DBConstant.FIELD_CATEGORY, newCategoryName);

		int updatedRecordsInCategories = db.update(
				DBConstant.TABLE_NAME_CATEGORIES, cv, DBConstant.FIELD_CATEGORY
						+ " = '" + oldCategoryName + "'", null);
		System.out.println("Updated In Categories: "
				+ updatedRecordsInCategories);
		int updatedRecordsInContacts = db.update(
				DBConstant.TABLE_NAME_CONTACTS, cv, DBConstant.FIELD_CATEGORY
						+ " = '" + oldCategoryName + "'", null);
		System.out.println("Updated In Contacts: " + updatedRecordsInContacts);
		db.close();
	}

	/**
	 * Создание новой категории
	 * 
	 * @param context
	 *            контекст
	 * @param newCategoryName
	 *            имя новой категории
	 */
	public static void addCategory(Context context, String newCategoryName) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(DBConstant.FIELD_CATEGORY, newCategoryName);
		db.insert(DBConstant.TABLE_NAME_CATEGORIES, null, cv);
		db.close();
	}

	/**
	 * Получение списка категорий
	 * 
	 * @param context
	 *            контекст
	 * @return ArrayList<String> список с категориями
	 */
	public static ArrayList<String> getCategoriesList(Context context) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		Cursor c = db.query(DBConstant.TABLE_NAME_CATEGORIES, null, null, null,
				null, null, null, null);
		ArrayList<String> categories = new ArrayList<String>();
		if (c.moveToFirst()) {
			do {
				int fCategory = c.getColumnIndex(DBConstant.FIELD_CATEGORY);
				String category = c.getString(fCategory);
				System.out.println("Category" + category);
				categories.add(category);
			} while (c.moveToNext());
		} else {
			System.out.println("Нет категорий");
		}
		db.close();
		return categories;
	}

	// *****************CATEGORIES*********************//

	// *****************FAVORITES*********************//

	/**
	 * Данные дублируются
	 */
	public static int DUPLICATE_DATA_ERROR_CODE = 2;
	/**
	 * Ошибка при работе с базой
	 */
	public static int DATA_BASE_ERROR = 1;
	/**
	 * Нет ошибок
	 */
	public static int NO_ERRORS = 0;

	/**
	 * Добавление контакта в избранное
	 * 
	 * @param context
	 *            контекст
	 * @param name
	 *            имя
	 * @param surname
	 *            фамилия
	 * @param patronymic
	 *            отчество
	 * @param phone
	 *            телефон
	 * @param category
	 *            категория
	 * @param dateOfBirth
	 *            дата рождения
	 * @param picture
	 *            URI изображения
	 * @return int error code
	 */
	public static int addContactToFavorites(Context context, String name,
			String surname, String patronymic, String phone, String category,
			String dateOfBirth, String picture) {
		int errorCode = NO_ERRORS;
		if (isDuplicated(context, name, surname, patronymic, phone, category,
				dateOfBirth, picture)) {
			errorCode = DUPLICATE_DATA_ERROR_CODE;
		} else {
			ContentValues cv = new ContentValues();
			cv.put(DBConstant.FIELD_NAME, name);
			cv.put(DBConstant.FIELD_SURNAME, surname);
			cv.put(DBConstant.FIELD_PATRONYMIC, patronymic);
			cv.put(DBConstant.FIELD_PHONE, phone);
			cv.put(DBConstant.FIELD_CATEGORY, category);
			cv.put(DBConstant.FIELD_DATE_OF_BIRTH, dateOfBirth);
			cv.put(DBConstant.FIELD_PICTURE, picture);

			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			try {
				long id = db.insertOrThrow(DBConstant.TABLE_NAME_FAVORITES,
						null, cv);
				Log.i(LOG_TAG, "Запись успешно добавлена. ID - " + id);
			} catch (SQLiteException e) {
				errorCode = DATA_BASE_ERROR;
				Log.e(LOG_TAG, "Ошибка при добавлении записи. STACRTRACE:");
				e.printStackTrace();
			}
			db.close();
		}
		return errorCode;
	}

	/**
	 * Получает список избранных контактов
	 * 
	 * @param context
	 *            контекст
	 * @return ArrayList<HashMap<String, String>> список всех избранные
	 *         контактов
	 */
	public static ArrayList<HashMap<String, String>> getAllFavorites(
			Context context) {
		ArrayList<HashMap<String, String>> contacts = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		Cursor c = db.query(DBConstant.TABLE_NAME_FAVORITES, null, null, null,
				null, null, null, null);
		if (c.moveToFirst()) {
			do {
				int fId = c.getColumnIndex(DBConstant.FIELD_ID);
				int fName = c.getColumnIndex(DBConstant.FIELD_NAME);
				int fSurname = c.getColumnIndex(DBConstant.FIELD_SURNAME);
				int fPatronymic = c.getColumnIndex(DBConstant.FIELD_PATRONYMIC);
				int fDateOfBirth = c
						.getColumnIndex(DBConstant.FIELD_DATE_OF_BIRTH);
				int fPicture = c.getColumnIndex(DBConstant.FIELD_PICTURE);
				int fPhone = c.getColumnIndex(DBConstant.FIELD_PHONE);
				int fCategory = c.getColumnIndex(DBConstant.FIELD_CATEGORY);

				String id = c.getString(fId);
				String name = c.getString(fName);
				String surname = c.getString(fSurname);
				String patronymic = c.getString(fPatronymic);
				String dateOfBirth = c.getString(fDateOfBirth);
				String picture = c.getString(fPicture);
				String phone = c.getString(fPhone);
				String category = c.getString(fCategory);

				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put(DBConstant.FIELD_ID, id);
				temp.put(DBConstant.FIELD_NAME, name);
				temp.put(DBConstant.FIELD_SURNAME, surname);
				temp.put(DBConstant.FIELD_PATRONYMIC, patronymic);
				temp.put(DBConstant.FIELD_DATE_OF_BIRTH, dateOfBirth);
				temp.put(DBConstant.FIELD_PICTURE, picture);
				temp.put(DBConstant.FIELD_PHONE, phone);
				temp.put(DBConstant.FIELD_CATEGORY, category);
				contacts.add(temp);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return contacts;
	}

	/**
	 * Удаляет контакт из избранных
	 * 
	 * @param context
	 *            контекст
	 * @param id
	 *            ИД удаляемого контакта
	 */
	public static void deleteFromFavoritesContact(Context context, String id) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		try {
			db.delete(DBConstant.TABLE_NAME_FAVORITES, DBConstant.FIELD_ID
					+ " = '" + id + "'", null);
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		db.close();
	}

	/**
	 * Удаляет контакт из избранных. <br>
	 * При удалении контакта из основной телефонной книги, он должен быть удален
	 * и из избранного. Этот метод позволяет удалить контакт по его полям
	 * 
	 * @param context
	 *            контекст
	 * @param name
	 *            имя
	 * @param surname
	 *            фамилия
	 * @param patronymic
	 *            отчество
	 * @param phone
	 *            телефон
	 * @param category
	 *            категория
	 * @param dateOfBirth
	 *            дата рождения
	 * @param picture
	 *            URI изображения
	 */
	/*

	 */
	public static void deleteFromFavoritesContact(Context context, String name,
			String surname, String patronymic, String phone, String category,
			String dateOfBirth, String picture) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();

		// MAGIC! DO NOT TOUCH!!!!
		String where = "((" + DBConstant.FIELD_NAME + " = '" + name + "') and "
				+ "(" + DBConstant.FIELD_SURNAME + " = '" + surname + "') and "
				+ "(" + DBConstant.FIELD_PATRONYMIC + " = '" + patronymic
				+ "') and " + "(" + DBConstant.FIELD_PHONE + " = '" + phone
				+ "') and " + "(" + DBConstant.FIELD_CATEGORY + " = '"
				+ category + "') and " + "(" + DBConstant.FIELD_DATE_OF_BIRTH
				+ " = '" + dateOfBirth + "') and " + "("
				+ DBConstant.FIELD_PICTURE + " = '" + picture + "'));";
		try {
			db.delete(DBConstant.TABLE_NAME_FAVORITES, where, null);
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		db.close();
	}

	/**
	 * Проверяет, существует ли контакт в избранном с такими же данными
	 * 
	 * @param context
	 *            контекст
	 * @param name
	 *            имя
	 * @param surname
	 *            фамилия
	 * @param patronymic
	 *            отчество
	 * @param phone
	 *            телефон
	 * @param category
	 *            категория
	 * @param dateOfBirth
	 *            дата рождения
	 * @param picture
	 *            URI изображения
	 * @return true - такой контакт уже в избранном
	 */
	public static boolean isDuplicated(Context context, String name,
			String surname, String patronymic, String phone, String category,
			String dateOfBirth, String picture) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		// MAGIC! DO NOT TOUCH!!!!
		String where = "((" + DBConstant.FIELD_NAME + " = '" + name + "') and "
				+ "(" + DBConstant.FIELD_SURNAME + " = '" + surname + "') and "
				+ "(" + DBConstant.FIELD_PATRONYMIC + " = '" + patronymic
				+ "') and " + "(" + DBConstant.FIELD_PHONE + " = '" + phone
				+ "') and " + "(" + DBConstant.FIELD_CATEGORY + " = '"
				+ category + "') and " + "(" + DBConstant.FIELD_DATE_OF_BIRTH
				+ " = '" + dateOfBirth + "') and " + "("
				+ DBConstant.FIELD_PICTURE + " = '" + picture + "'));";

		Cursor c = db.query(DBConstant.TABLE_NAME_FAVORITES, null, where, null,
				null, null, null, null);
		if (c.moveToFirst()) {
			db.close();
			return true;
		} else {
			// No data
		}
		db.close();
		return false;
	}

	/**
	 * Обновляет контакт в избранном. Этот метод используется при обычном
	 * обновлении контакта. Если контакт изменен, то данные в избранном об этом
	 * контакте также необходимо заменить. Этото метод позволяет найти контакт
	 * по его старым данным и заменить их на новые
	 * 
	 * @param context
	 *            контекст
	 * @param name
	 *            имя
	 * @param surname
	 *            фамилия
	 * @param patronymic
	 *            отчество
	 * @param phone
	 *            телефон
	 * @param category
	 *            категория
	 * @param dateOfBirth
	 *            дата рождения
	 * @param picture
	 *            URI изображения
	 * @param newData
	 *            новые данные
	 */
	public static void changeFavoriteContact(Context context, String name,
			String surname, String patronymic, String phone, String category,
			String dateOfBirth, String picture, Intent newData) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();

		// MAGIC! DO NOT TOUCH!!!!
		String where = "((" + DBConstant.FIELD_NAME + " = '" + name + "') and "
				+ "(" + DBConstant.FIELD_SURNAME + " = '" + surname + "') and "
				+ "(" + DBConstant.FIELD_PATRONYMIC + " = '" + patronymic
				+ "') and " + "(" + DBConstant.FIELD_PHONE + " = '" + phone
				+ "') and " + "(" + DBConstant.FIELD_CATEGORY + " = '"
				+ category + "') and " + "(" + DBConstant.FIELD_DATE_OF_BIRTH
				+ " = '" + dateOfBirth + "') and " + "("
				+ DBConstant.FIELD_PICTURE + " = '" + picture + "'));";

		System.out.println("WHERE = " + where);

		ContentValues cv = new ContentValues();
		cv.put(DBConstant.FIELD_NAME,
				newData.getStringExtra(Constant.EXTRA_CONTACT_NAME));
		cv.put(DBConstant.FIELD_SURNAME,
				newData.getStringExtra(Constant.EXTRA_CONTACT_SURNAME));
		cv.put(DBConstant.FIELD_PATRONYMIC,
				newData.getStringExtra(Constant.EXTRA_CONTACT_PATRONYMIC));
		cv.put(DBConstant.FIELD_PHONE,
				newData.getStringExtra(Constant.EXTRA_CONTACT_PHONE));
		cv.put(DBConstant.FIELD_CATEGORY,
				newData.getStringExtra(Constant.EXTRA_CONTACT_CATEGORY));
		cv.put(DBConstant.FIELD_DATE_OF_BIRTH,
				newData.getStringExtra(Constant.EXTRA_CONTACT_DATE_OF_BIRTH));
		cv.put(DBConstant.FIELD_PICTURE,
				newData.getStringExtra(Constant.EXTRA_CONTACT_PICTURE));

		try {
			int id = db
					.update(DBConstant.TABLE_NAME_FAVORITES, cv, where, null);
			System.out.println("обновлено " + id);
		} catch (SQLiteException e) {
			Log.e(LOG_TAG, "Ошибка при добавлении записи. STACRTRACE:");
			e.printStackTrace();
		}
		db.close();
	}
	// *****************FAVORITES*********************//
}
