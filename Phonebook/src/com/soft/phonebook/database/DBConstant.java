package com.soft.phonebook.database;

/**
 * Константы для базы данных
 * @author Nickolai
 *
 */
public class DBConstant {
	public static final String DATA_BASE_NAME = "Phonebook";

	public static final String TABLE_NAME_CONTACTS = "contacts";
	public static final String TABLE_NAME_CATEGORIES = "categories";
	public static final String TABLE_NAME_FAVORITES = "favorites";
	
	public static final String FIELD_ID = "_id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_SURNAME = "surname";
	public static final String FIELD_PATRONYMIC = "patronymic";
	public static final String FIELD_PHONE = "phone";
	public static final String FIELD_DATE_OF_BIRTH = "date_of_birth";
	public static final String FIELD_PICTURE = "picture";
	public static final String FIELD_CATEGORY = "category";
	
}
