package com.soft.phonebook.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Создает базу данных
 * @author Nickolai
 *
 */
public class DAO extends SQLiteOpenHelper {

	/**
	 * Конструктор
	 * @param context контекст
	 */
	public DAO(Context context) {
		super(context, DBConstant.DATA_BASE_NAME, null, 2);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table " + DBConstant.TABLE_NAME_CONTACTS + " ("
				+ DBConstant.FIELD_ID + " integer primary key autoincrement,"
				+ DBConstant.FIELD_NAME + " text, " + DBConstant.FIELD_SURNAME
				+ " text," + DBConstant.FIELD_PATRONYMIC + " text,"
				+ DBConstant.FIELD_CATEGORY + " text,"
				+ DBConstant.FIELD_DATE_OF_BIRTH + " text,"
				+ DBConstant.FIELD_PHONE + "  text, "
				+ DBConstant.FIELD_PICTURE + " text);");
		
		db.execSQL("create table " + DBConstant.TABLE_NAME_FAVORITES + " ("
				+ DBConstant.FIELD_ID + " integer primary key autoincrement,"
				+ DBConstant.FIELD_NAME + " text, " + DBConstant.FIELD_SURNAME
				+ " text," + DBConstant.FIELD_PATRONYMIC + " text,"
				+ DBConstant.FIELD_CATEGORY + " text,"
				+ DBConstant.FIELD_DATE_OF_BIRTH + " text,"
				+ DBConstant.FIELD_PHONE + "  text, "
				+ DBConstant.FIELD_PICTURE + " text);");

		db.execSQL("create table " + DBConstant.TABLE_NAME_CATEGORIES + " ("
				+ DBConstant.FIELD_ID + " integer primary key autoincrement,"
				+ DBConstant.FIELD_CATEGORY + " text);");
		db.execSQL("INSERT INTO "+DBConstant.TABLE_NAME_CATEGORIES + " (`"+DBConstant.FIELD_CATEGORY+"`) values ('Без категории') ;");
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
