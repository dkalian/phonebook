package com.soft.phonebook;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.database.DBOperations;

/**
 * Контекстное меню для избранного контакта
 * 
 * @author Nickolai
 * 
 */
public class FavoriteSelectAction extends Activity {

	Button deleteFromFavorite, cancel;

	Bundle extras;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_favorite_select_action);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		deleteFromFavorite = (Button) findViewById(R.id.activity_favorite_select_action_delete_from_favorite);
		cancel = (Button) findViewById(R.id.activity_favorite_select_action_cancel);

		extras = getIntent().getExtras();

		deleteFromFavorite.setOnClickListener(deleteFromFavoriteClickListener);
		cancel.setOnClickListener(cancelClickListener);
	}

	OnClickListener deleteFromFavoriteClickListener = new OnClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			DBOperations.deleteFromFavoritesContact(FavoriteSelectAction.this,
					extras.getString(Constant.EXTRA_CONTACT_ID));
			setResult(RESULT_OK);
			finish();
		}
	};

	OnClickListener cancelClickListener = new OnClickListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			setResult(RESULT_CANCELED);
			finish();
		}
	};

}
