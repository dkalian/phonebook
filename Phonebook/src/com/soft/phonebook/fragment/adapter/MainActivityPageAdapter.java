package com.soft.phonebook.fragment.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.soft.phonebook.fragment.AllFragment;
import com.soft.phonebook.fragment.CategoriesFragment;
import com.soft.phonebook.fragment.FavoritesFragment;

/**
 * Адаптер для PageView`ера
 * 
 * @author Nickolai
 * 
 */
public class MainActivityPageAdapter extends FragmentPagerAdapter {

	Fragment categoriesFragment;
	Fragment allFragment;
	Fragment favoritesFragment;

	/**
	 * Констуктор
	 * 
	 * @param fm
	 *            FragmentManager
	 */
	public MainActivityPageAdapter(FragmentManager fm) {
		super(fm);
		categoriesFragment = CategoriesFragment.newInstance(0);
		allFragment = AllFragment.newInstance(1);
		favoritesFragment = FavoritesFragment.newInstance(2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
	 */
	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return categoriesFragment;
		case 1:
			return allFragment;
		case 2:
			return favoritesFragment;
		default:
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.view.PagerAdapter#getPageTitle(int)
	 */
	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "Категории";

		case 1:

			return "Все";

		case 2:

			return "Избранное";

		default:
			break;
		}
		return super.getPageTitle(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.view.PagerAdapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

}
