package com.soft.phonebook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.soft.phonebook.AddContact;
import com.soft.phonebook.Contact;
import com.soft.phonebook.ContactSelectAction;
import com.soft.phonebook.R;
import com.soft.phonebook.adapter.AllContactsAdapter;
import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.constant.ResultCodes;
import com.soft.phonebook.database.DBConstant;
import com.soft.phonebook.database.DBOperations;
import com.soft.phonebook.design.CustomToast;

/**
 * Фрагмент экрана со списком "все"
 * 
 * @author Nickolai
 * 
 */
public class AllFragment extends Fragment {

	/**
	 * @param page
	 *            номер страницы
	 * @return Fragment созданный фрагмент
	 */
	public static Fragment newInstance(int page) {
		AllFragment allFragment = new AllFragment();
		return allFragment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		allContactsAdapter = new AllContactsAdapter(getActivity());
		setHasOptionsMenu(true);
	}

	ListView contactsList;
	AllContactsAdapter allContactsAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		allContactsAdapter.notifyDataSetChanged();
		super.onStart();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_all, null);
		contactsList = (ListView) view
				.findViewById(R.id.fragment_all_contacts_list);
		contactsList.setAdapter(allContactsAdapter);
		contactsList.setOnItemLongClickListener(itemLongClickListener);
		contactsList.setOnItemClickListener(itemClickListener);
		return view;
	}

	OnItemClickListener itemClickListener = new OnItemClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.widget.AdapterView.OnItemClickListener#onItemClick(android
		 * .widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
				long arg3) {
			Intent data = new Intent();
			data.putExtra(Constant.EXTRA_CONTACT_ID, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_ID));
			data.putExtra(Constant.EXTRA_CONTACT_NAME, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_NAME));
			data.putExtra(Constant.EXTRA_CONTACT_SURNAME, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_SURNAME));
			data.putExtra(Constant.EXTRA_CONTACT_PATRONYMIC, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_PATRONYMIC));
			data.putExtra(Constant.EXTRA_CONTACT_PHONE, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_PHONE));
			data.putExtra(Constant.EXTRA_CONTACT_PICTURE, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_PICTURE));
			data.putExtra(Constant.EXTRA_CONTACT_CATEGORY, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_CATEGORY));
			data.putExtra(
					Constant.EXTRA_CONTACT_DATE_OF_BIRTH,
					allContactsAdapter.getItem(pos).get(
							DBConstant.FIELD_DATE_OF_BIRTH));
			startActivityForResult(
					new Intent(getActivity(), Contact.class).putExtras(data), 1);
		}
	};

	OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.widget.AdapterView.OnItemLongClickListener#onItemLongClick
		 * (android.widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos,
				long id) {
			Intent data = new Intent();
			data.putExtra(Constant.EXTRA_CONTACT_ID, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_ID));
			data.putExtra(Constant.EXTRA_CONTACT_NAME, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_NAME));
			data.putExtra(Constant.EXTRA_CONTACT_SURNAME, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_SURNAME));
			data.putExtra(Constant.EXTRA_CONTACT_PATRONYMIC, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_PATRONYMIC));
			data.putExtra(Constant.EXTRA_CONTACT_PHONE, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_PHONE));
			data.putExtra(Constant.EXTRA_CONTACT_PICTURE, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_PICTURE));
			data.putExtra(Constant.EXTRA_CONTACT_CATEGORY, allContactsAdapter
					.getItem(pos).get(DBConstant.FIELD_CATEGORY));
			data.putExtra(
					Constant.EXTRA_CONTACT_DATE_OF_BIRTH,
					allContactsAdapter.getItem(pos).get(
							DBConstant.FIELD_DATE_OF_BIRTH));
			startActivityForResult(new Intent(getActivity(),
					ContactSelectAction.class).putExtras(data), 1);
			return false;
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#setUserVisibleHint(boolean)
	 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (allContactsAdapter != null) {
			allContactsAdapter.updateDataSet();
			allContactsAdapter.notifyDataSetChanged();
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateOptionsMenu(android.view.Menu,
	 * android.view.MenuInflater)
	 */
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.all_contacts_menu, menu);
		MenuItem add = menu.findItem(R.id.all_contacts_menu_add);
		add.setOnMenuItemClickListener(addClickListener);

		MenuItem searchItem = menu.findItem(R.id.all_contacts_menu_search);
		SearchView searchView = (SearchView) MenuItemCompat
				.getActionView(searchItem);
		searchView.setQueryHint("Поиск");
		searchView.setOnQueryTextListener(onQueryTextListener);
		searchView.setOnCloseListener(new OnCloseListener() {

			@Override
			public boolean onClose() {
				return false;
			}
		});
		super.onCreateOptionsMenu(menu, inflater);
	}

	OnQueryTextListener onQueryTextListener = new OnQueryTextListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v7.widget.SearchView.OnQueryTextListener#
		 * onQueryTextSubmit(java.lang.String)
		 */
		@Override
		public boolean onQueryTextSubmit(String string) {
			// TODO Auto-generated method stub
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v7.widget.SearchView.OnQueryTextListener#
		 * onQueryTextChange(java.lang.String)
		 */
		@Override
		public boolean onQueryTextChange(String string) {
			allContactsAdapter.getFilter().filter(string);
			return false;
		}
	};

	OnMenuItemClickListener addClickListener = new OnMenuItemClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.view.MenuItem.OnMenuItemClickListener#onMenuItemClick(android
		 * .view.MenuItem)
		 */
		@Override
		public boolean onMenuItemClick(MenuItem arg0) {
			startActivityForResult(new Intent(getActivity(), AddContact.class),
					1);
			return false;
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case ResultCodes.CONTACT_SELECT_ACTION_ACTION_TO_FAVORITES: {
			if (data.getExtras().getInt(Constant.EXTRA_ERROR_CODE) == DBOperations.DUPLICATE_DATA_ERROR_CODE) {
				CustomToast.getInfoToast(getActivity(), "Уже в избранном",
						Toast.LENGTH_LONG).show();
			}
		}
		default:
			break;
		}
		allContactsAdapter.updateDataSet();
		allContactsAdapter.notifyDataSetChanged();

	};

}
