package com.soft.phonebook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.soft.phonebook.CategorySelectAction;
import com.soft.phonebook.CreateCategory;
import com.soft.phonebook.R;
import com.soft.phonebook.adapter.CategoriesAdapter;
import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.constant.ResultCodes;
import com.soft.phonebook.database.DBOperations;
import com.soft.phonebook.design.CustomToast;

/**
 * Фрагмент экрана со списком "категории"
 * 
 * @author Nickolai
 * 
 */
public class CategoriesFragment extends Fragment {
	/**
	 * @param page
	 *            номер страницы
	 * @return Fragment созданный фрагмент
	 */
	public static Fragment newInstance(int page) {
		CategoriesFragment categoriesFragment = new CategoriesFragment();
		return categoriesFragment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	ExpandableListView categoriesList;
	CategoriesAdapter categoriesAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_categories, null);
		categoriesList = (ExpandableListView) view
				.findViewById(R.id.fragment_categories_categories_list);
		categoriesAdapter = new CategoriesAdapter(getActivity());
		categoriesList.setAdapter(categoriesAdapter);
		categoriesList.setOnItemLongClickListener(itemLongClickListener);
		return view;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#setUserVisibleHint(boolean)
	 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (categoriesAdapter != null) {
			categoriesAdapter.updateDataSet();
			categoriesAdapter.notifyDataSetChanged();
		}
	};

	OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.widget.AdapterView.OnItemLongClickListener#onItemLongClick
		 * (android.widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos,
				long id) {
			System.out.println("pos = " + pos + " id = " + id);
			if (id == 0 && pos == 0) {
				CustomToast.getInfoToast(getActivity(),
						"Эту категорию изменить нельзя", Toast.LENGTH_LONG)
						.show();
			}
			if (id > 0) { // Group element
				if (categoriesAdapter.getCategoryNameByPosition(pos).compareTo(
						"Без категории") != 0) {
					startActivityForResult(new Intent(getActivity(),
							CategorySelectAction.class).putExtra(
							Constant.EXTRA_SELECTED_CATEGORY_NAME,
							categoriesAdapter.getCategoryNameByPosition(pos)),
							1);
				} else { // Child element

				}
			}
			return false;
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateOptionsMenu(android.view.Menu,
	 * android.view.MenuInflater)
	 */
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.categories_menu, menu);
		MenuItem add = menu.findItem(R.id.categories_menu_add_category);
		add.setOnMenuItemClickListener(addClickListener);
		super.onCreateOptionsMenu(menu, inflater);
	}

	OnMenuItemClickListener addClickListener = new OnMenuItemClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.view.MenuItem.OnMenuItemClickListener#onMenuItemClick(android
		 * .view.MenuItem)
		 */
		@Override
		public boolean onMenuItemClick(MenuItem arg0) {
			startActivityForResult(new Intent(getActivity(),
					CreateCategory.class), 1);
			return false;
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("RESULT CODE:" + requestCode);
		switch (resultCode) {
		case ResultCodes.CREATE_CATEGORY: // Создание категории
			System.out.println("CREATE CATEGORY ON RESULT");
			String newCategoryName = data.getExtras().getString(
					Constant.EXTRA_NEW_CATEGORY_NAME);
			System.out.println("New category name" + newCategoryName);
			DBOperations.addCategory(getActivity(), newCategoryName);
			categoriesAdapter.updateDataSet();
			categoriesAdapter.notifyDataSetChanged();
			break;
		case ResultCodes.CATEGORY_ACTION_CHANGE: { // Изменение категории

			// DBOperations.deleteCategory(CategorySelectAction.this,
			// categoryName);
			categoriesAdapter.updateDataSet();
			categoriesAdapter.notifyDataSetChanged();
			break;
		}
		case ResultCodes.CATEGORY_ACTION_DELETE: { // Удаление категории
			categoriesAdapter.updateDataSet();
			categoriesAdapter.notifyDataSetChanged();
		}
		default:
			break;
		}
	};
}
