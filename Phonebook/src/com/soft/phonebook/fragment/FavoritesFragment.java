package com.soft.phonebook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.soft.phonebook.FavoriteSelectAction;
import com.soft.phonebook.R;
import com.soft.phonebook.adapter.FavoritesAdapter;
import com.soft.phonebook.constant.Constant;
import com.soft.phonebook.database.DBConstant;

/**
 * Фрагмент экрана со списком "избранное"
 * 
 * @author Nickolai
 * 
 */
public class FavoritesFragment extends Fragment {
	/**
	 * @param page
	 *            номер страницы
	 * @return Fragment созданный фрагмент
	 */
	public static Fragment newInstance(int page) {
		FavoritesFragment favoritesFragment = new FavoritesFragment();
		return favoritesFragment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	ListView favoritesList;
	FavoritesAdapter favoritesAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_favorites, null);
		favoritesList = (ListView) view
				.findViewById(R.id.fragment_favorites_favorites_list);
		favoritesAdapter = new FavoritesAdapter(getActivity());
		favoritesList.setAdapter(favoritesAdapter);
		favoritesList.setOnItemLongClickListener(itemLongClickListener);
		return view;
	}

	OnItemClickListener itemClickListener = new OnItemClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.widget.AdapterView.OnItemClickListener#onItemClick(android
		 * .widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {

		}
	};

	OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.widget.AdapterView.OnItemLongClickListener#onItemLongClick
		 * (android.widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos,
				long id) {
			startActivityForResult(new Intent(getActivity(),
					FavoriteSelectAction.class).putExtra(
					Constant.EXTRA_CONTACT_ID, favoritesAdapter.getItem(pos)
							.get(DBConstant.FIELD_ID)), 1);
			return false;
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#setUserVisibleHint(boolean)
	 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (favoritesAdapter != null) {
			favoritesAdapter.updateDataSet();
			favoritesAdapter.notifyDataSetChanged();
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		favoritesAdapter.updateDataSet();
		favoritesAdapter.notifyDataSetChanged();
	}
}
